
/*
   -- New project --
   
   This source code of graphical user interface 
   has been generated automatically by RemoteXY editor.
   To compile this code using RemoteXY library 3.1.13 or later version 
   download by link http://remotexy.com/en/library/
   To connect using RemoteXY mobile app by link http://remotexy.com/en/download/                   
     - for ANDROID 4.15.01 or later version;
     - for iOS 1.12.1 or later version;
    
   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.    
*/

//////////////////////////////////////////////
//        RemoteXY include library          //
//////////////////////////////////////////////

// you can enable debug logging to Serial at 115200
//#define REMOTEXY__DEBUGLOG    

// RemoteXY select connection mode and include library 
#define REMOTEXY_MODE__SOFTSERIAL

#include <SoftwareSerial.h>

// RemoteXY connection settings 
#define REMOTEXY_SERIAL_RX 0
#define REMOTEXY_SERIAL_TX 1
#define REMOTEXY_SERIAL_SPEED 9600


#include <RemoteXY.h>
#include <TB6612FNG.h>


// RemoteXY GUI configuration  
#pragma pack(push, 1)  
uint8_t RemoteXY_CONF[] =   // 88 bytes
  { 255,2,0,7,0,81,0,19,0,0,0,0,31,1,106,200,1,1,5,0,
  5,23,107,60,60,21,2,26,31,71,30,7,46,46,56,0,2,24,135,0,
  0,0,0,0,0,200,66,0,0,160,65,0,0,32,65,0,0,0,64,24,
  0,70,11,65,18,18,16,26,37,0,70,77,65,18,18,16,26,37,0,70,
  44,65,18,18,16,26,37,0 };
  
// this structure defines all the variables and events of your control interface 
struct {

    // input variables
  int8_t joystick_01_x; // from -100 to 100
  int8_t joystick_01_y; // from -100 to 100

    // output variables
  float Distance_sensor; // from 0 to 100
  uint8_t led_01; // from 0 to 1
  uint8_t led_02; // from 0 to 1
  uint8_t led_03; // from 0 to 1

    // other variable
  uint8_t connect_flag;  // =1 if wire connected, else =0

} RemoteXY;   
#pragma pack(pop)
 
/////////////////////////////////////////////
//           END RemoteXY include          //
/////////////////////////////////////////////
TB6612FNG tb_alx(5,4,6,7);
int value_x;
float duration;
const int trigPin = A0;
const int echoPin = 2;



void setup() 
{
  RemoteXY_Init (); 
  
  // TODO you setup code
  pinMode(trigPin, OUTPUT);  
	pinMode(echoPin, INPUT);
}

void loop() 
{ 
  RemoteXY_Handler ();
  
  // TODO you loop code
  // use the RemoteXY structure for data transfer
  // do not call delay(), use instead RemoteXY_delay() 
  //  joystick_01_y>0 ===> Move forward
  if (RemoteXY.joystick_01_y>0) {
    if (RemoteXY.joystick_01_x>0) {
        value_x = map(RemoteXY.joystick_01_x, 0, 100, 0, 50);
        tb_alx.setSpeed(M1, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)-value_x);
	      tb_alx.setSpeed(M2, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)+value_x);
    }
    else if (RemoteXY.joystick_01_x<0) {
        value_x = map((-1)*RemoteXY.joystick_01_x, 0, 100, 0, 50);
        tb_alx.setSpeed(M1, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)+value_x);
	      tb_alx.setSpeed(M2, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)-value_x);
    }
    else { 
        value_x = 0;
        tb_alx.setSpeed(M1, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200));
	      tb_alx.setSpeed(M2, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200));
    }

  } else if (RemoteXY.joystick_01_y<0) {
    if (RemoteXY.joystick_01_x>0) {
        value_x = map(RemoteXY.joystick_01_x, 0, 100, 0, 50);
        tb_alx.setSpeed(M1, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)-value_x);
	      tb_alx.setSpeed(M2, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)+value_x);
    }
    else if (RemoteXY.joystick_01_x<0) {
        value_x = map((-1)*RemoteXY.joystick_01_x, 0, 100, 0, 50);
        tb_alx.setSpeed(M1, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)+value_x);
	      tb_alx.setSpeed(M2, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200)-value_x);
    }
    else { 
        value_x = 0;
        tb_alx.setSpeed(M1, CW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200));
	      tb_alx.setSpeed(M2, CCW, map(RemoteXY.joystick_01_y, 0, 100, 0, 200));
    }
  } else {
    tb_alx.setSpeed(M1, CCW, 0);
	  tb_alx.setSpeed(M2, CW, 0);
  }

  RemoteXY.led_01 = digitalRead(A1);
  RemoteXY.led_02 = digitalRead(A2);
  RemoteXY.led_03 = digitalRead(A3);

  // HC-SR04 specific
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  RemoteXY.Distance_sensor = (duration*.0343)/2;
  delay(100);
}