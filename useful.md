# Polytech R2 kit - Χρήσιμες πληροφορίες


## Πως χρησιμοποιώ το πρόσθετο αυτό;

Υπάρχουν 2 τρόποι: 

* Με εγκατάσταση του πρόσθετου στο Mind+. Σε περίπτωση που θέλεις να το εγκαταστήσεις με τον "κλασικό" τρόπο, ο τρόπος εγκατάστασης του πρόσθετου για το R2, είναι ακριβώς ίδιος με τον τρόπο εγκατάστασης του πρόσθετου για το S1 για το οποίο έχω ετοιμάσει βίντεο. [Βίντεο εγκατάστασης του πρόσθετου για το S1](https://www.youtube.com/watch?v=fQELPj0-dTM). Σε κάθε περίπτωση η εγκατάσταση του πρόσθετου είναι προαιρετική.

* Με χρήση ενός αρχείου έργου (saved project) στο οποίο έχει ήδη ενσωματωθεί το πρόσθετο. Σε έναν υπολογιστή που είναι ήδη εγκατεστημένο το Mind+ μπορείς να κατεβάσεις ένα ήδη έτοιμο έργο που έχει ενσωματωμένο το πρόσθετο. Ένα τέτοιο αποθηκευμένο έργο, με την τελευταία έκδοση του πρόσθετου, είναι διαθέσιμο [εδώ](https://ale3andro.gr/blog/wp-content/uploads/R2.mp). Περισσότερα για το θέμα [εδώ](https://ale3andro.gr/blog/2024/11/26/mind-%ce%b1%cf%80%ce%bf%ce%b8%ce%b7%ce%ba%ce%b5%cf%85%ce%bc%ce%ad%ce%bd%ce%b1-%ce%ad%cf%81%ce%b3%ce%b1-%ce%ba%ce%b1%ce%b9-%cf%80%cf%81%cf%8c%cf%83%ce%b8%ce%b5%cf%84%ce%b1/)


## Ποια είναι η σύμβαση για τον χαρακτηρισμό των κινητήρων και αισθητήρων ως "δεξιός", "αριστερός" και "κεντρικός" και για την κατεύθυνση στροφής ως "ρολογιού" και "αντίστροφη ρολογιού"; 

Ο χαρακτηρισμός των κινητήρων και των αισθητήρων αντανάκλασης βασίζεται στην παρατήρηση του R2 από την πίσω του πλευρά. Δες την παρακάτω εικόνα.

![](./arduinoC/_images/R2_from_the_back.png)

Στη φάση της συναρμολόγησης του R2, θα πρέπει ο **δεξιός κινητήρας να συνδεθεί στην υποδοχή ΜΑ** του Arduino και ο **αριστερός κινητήρας να συνδεθεί στην υποδοχή MB** του Arduino. Δες την παρακάτω εικόνα.

![](./arduinoC/_images/R2_motor_connection.png)

Ο χαρακτηρισμός της κατεύθυνσης στροφής των τροφών ως "ρολογιού" και "αντίστροφη ρολογιού" είναι αυτή που χρησιμοποιείται στη Φυσική (Γυμνασίου). Δες την παρακάτω εικόνα.

![](./arduinoC/_images/R2_wheel_rotation.jpg)

---------------------------------------------------------

## Μοντέλο 3d για τα κλιπ (τύπου lego) συγκράτησης των αισθητήρων και ενεργοποιητών

![](./arduinoC/_images/polytech_shield_holder.png)

[Διαθέσιμο εδώ](https://www.thingiverse.com/thing:6786623)

---------------------------------------------------------

## Πως μπορώ να αλλάξω το όνομα του BT-24 bluetooth dongle

Αυτό ενδιαφέρει όσους συναδέλφους ενδιαφέρονται να συνδέσουν το R2 με την Android εφαρμογή της Polytech.

Περισσότερες πληροφορίες [εδώ](https://ale3andro.gr/blog/2024/11/01/%ce%b1%ce%bb%ce%bb%ce%b1%ce%b3%ce%ae-%ce%bf%ce%bd%cf%8c%ce%bc%ce%b1%cf%84%ce%bf%cf%82-bluetooth-%cf%83%cf%84%ce%bf-polytech-r2/)

Χρήσιμα αρχεία: 

(0) To ino αρχείο (Arduino sketch) που πρέπει να φορτωθεί στο R2 για να λειτουργεί ο χειρισμός μέσω του Android app είναι διαθέσιμο [εδώ](https://mega.nz/file/wNYClAyZ#fxHaCWxnH5Zp9GUMGh04-xT1rMThxh_4tkm_GfI17s8). 
**Σημαντικό** Το παραπάνω αρχείο πρέπει να φορτωθεί μέσω Arduino IDE και να έχει αφαιρεθεί το Bluetooth Dongle πριν το upload

(1) Το Android app για το R2 είναι διαθέσιμο [εδώ](https://mega.nz/file/BNgSHCzZ#kaPzwUby_ivAC4FpPmMsG2MjApfdV6NbyiIyBDH9xuM)

---------------------------------------------------------

## Που μπορώ να βρω ενδεικτικά σενάρια;

[Εδώ](./endeiktika_senaria.md)

---------------------------------------------------------

## Τι είναι το κομμάτι Στείλε στο monitor app;

Το κομμάτι ![send to monitor app](./images/send_to_monitor_app.png) δημιουργήθηκε γιατί το κιτ δεν έχει (hardware) οθόνη. Χρησιμοποιείται σε συνδυασμό με το [monitor app](https://github.com/ale3andro/alx_arduino_monitor) ώστε να εμφανίζει τιμές από αισθητήρες σε πραγματικό χρόνο στην οθόνη του υπολογιστή (αν φυσικά το R2 είναι συνδεδεμένο με τον Υπολογιστή).Περισσότερες λεπτομέρειες για το θέμα [εδώ](https://ale3andro.gr/blog/2024/12/06/monitor-app-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b1-%ce%ba%ce%b9%cf%84-s1-%ce%ba%ce%b1%ce%b9-r2/)

---------------------------------------------------------

## Γιατί μετράς τις στροφές σε δευτερόλεπτα και όχι σε μοίρες;

Για να μάθεις τον λόγο, διάβασε [αυτό](https://ale3andro.gr/blog/2024/12/17/%ce%b5%ce%bd%ce%b7%ce%bc%ce%ad%cf%81%cf%89%cf%83%ce%b7-%cf%80%cf%81%cf%8c%cf%83%ce%b8%ce%b5%cf%84%ce%bf%cf%85-mind-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-%cf%81%ce%bf%ce%bc%cf%80%cf%8c%cf%84-r2/)

---------------------------------------------------------

## Μπορώ να χρησιμοποιήσω το R2 ως τηλεχειριζόμενο αυτοκίνητο μέσω Bluetooth;

Ναι γίνεται. Οδηγός [εδώ](https://ale3andro.gr/blog/2025/01/25/%cf%84%ce%b7%ce%bb%ce%b5%cf%87%ce%b5%ce%b9%cf%81%ce%b9%cf%83%ce%bc%cf%8c%cf%82-r2-%ce%bc%ce%b5-bluetooth-%ce%ba%ce%b1%ce%b9-%ce%b5%cf%86%ce%b1%cf%81%ce%bc%ce%bf%ce%b3%ce%ae-android/)
