\   
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum SPEED_VALUES {
    //% block="μικρή"
    100, 
    //% block="μεσαία"
    150,
    //% block="μεγάλη"
    200
}

enum MOTORS {
   //% block="Δεξιός"
   M1,
   //% block="Αριστερός"
   M2 
}

enum DIGITAL_PORTS {
    //% block="D8"
    8,
    //% block="D9"
    9, 
    //% block="D11"
    11,
    //% block="D12"
    12
}

enum ANALOG_PORTS {
    //% block="A0"
    0,
    //% block="A1"
    1,
    //% block="A2"
    2,
    //% block="A3"
    3,
}

enum DIRECTIONS {
    //% block="Ρολογιού"
    CCW,
    //% block="Αντίστροφη Ρολογιού"
    CW
}

enum LED_STATES {
    //% block="Άναψε"
    HIGH,
    //% block="Σβήσε"
    LOW
}

enum LINE_SENSORS {
    //% block="Δεξιός"
    A1,
    //% block="Κεντρικός"
    A2,
    //% block="Αριστερός"
    A3,
}

enum ANALOG_PINS_LIGHT_SENSOR {
    //% block="Α6"
    A6,
    //% block="Α1"
    A1,
    //% block="Α0"
    A0,
}

enum BUZZER_TONES_DURATION {
    //% block="Μισό"
    500,
    //% block="Τέταρτο"
    250,
    //% block="Όγδοο"
    125,
    //% block="Ολόκληρο"
    1000,
    //% block="Διπλό"
    2000,
    //% block="Στοπ"
    0,
}

enum NEOPIXEL_SUB_LEDS {
    //% block="Όλα"
    4,
    //% block="1o"
    0,
    //% block="2o"
    1,
    //% block="3o"
    2,
    //% block="4o"
    3,
}

enum BUZZER_TONES {
    //% block="Χαμηλό Ντο (C3)"
    131,
    //% block="Χαμηλό Ντο# (C#3)"
    139,
    //% block="Χαμηλό Ρε (D3)"
    147,
    //% block="Χαμηλό Ρε# (D#3)"
    156,
    //% block="Χαμηλό Μι (E3)"
    165,
    //% block="Χαμηλό Φα (C3)"
    175,
    //% block="Χαμηλό Φα# (F#3)"
    185,
    //% block="Χαμηλό Σολ (G3)"
    196,
    //% block="Χαμηλό Σολ# (G#3)"
    208,
    //% block="Χαμηλό Λα (A3)"
    220,
    //% block="Χαμηλό Λα# (A#3)"
    233,
    //% block="Χαμηλό Σι (B3)"
    247,
    //% block="Μεσαίο Ντο (C4)"
    262,
    //% block="Μεσαίο Ντο# (C#4)"
    277,
    //% block="Μεσαίο Ρε (D4)"
    294,
    //% block="Μεσαίο Ρε# (D#4)"
    311,
    //% block="Μεσαίο Μι (E4)"
    330,
    //% block="Μεσαίο Φα (F4)"
    349,
    //% block="Μεσαίο Φα# (F#4)"
    370,
    //% block="Μεσαίο Σολ (G4)"
    392,
    //% block="Μεσαίο Σολ# (G#4)"
    415,
    //% block="Μεσαίο Λα (A4)"
    440,
    //% block="Μεσαίο Λα# (A#4)"
    466,
    //% block="Μεσαίο Σι (B4)"
    494,
    //% block="Υψηλό Ντο (C5)"
    523,
    //% block="Υψηλό Ντο# (C#5)"
    554,
    //% block="Υψηλό Ρε (D5)"
    587,
    //% block="Υψηλό Ρε# (D#5)"
    622,
    //% block="Υψηλό Μι (E5)"
    659,
    //% block="Υψηλό Φα (F5)"
    698,
    //% block="Υψηλό Φα# (F#5)"
    740,
    //% block="Υψηλό Σολ (G5)"
    784,
    //% block="Υψηλό Σολ# (G#5)"
    831,
    //% block="Υψηλό Λα (A5)"
    880,
    //% block="Υψηλό Λα# (A#5)"
    932,
    //% block="Υψηλό Σι (B5)"
    988,
}

//% color="#0d6e2a" iconWidth=50 iconHeight=40
namespace alxR2 {
    //% block="Κινήσου προς τα μπροστά με ταχύτητα [SPEED]"
    //% SPEED.shadow="dropdown" SPEED.options="SPEED_VALUES" SPEED.defl="SPEED_VALUE.MAX"
    export function moveForward(parameter: any, block: any) {
        let speed = parameter.SPEED.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(M1, CCW, ${speed});`);
            Generator.addCode(`tb_alx.setSpeed(M2, CW, ${speed});`);
        }
    }

    //% block="Κινήσου προς τα πίσω με ταχύτητα [SPEED]"
    //% SPEED.shadow="dropdown" SPEED.options="SPEED_VALUES" SPEED.defl="SPEED_VALUE.MAX"
    export function moveBackward(parameter: any, block: any) {
        let speed = parameter.SPEED.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(M1, CW, ${speed});`);
            Generator.addCode(`tb_alx.setSpeed(M2, CCW, ${speed});`);
        }
    }

    //% block="Στρίψε δεξιά για [TIME] δευτερόλεπτα"
    //% TIME.shadow="number" TIME.defl="2"
    export function turnRight(parameter: any, block: any) {
        let time = parameter.TIME.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(M1, CW, 75);`);
            Generator.addCode(`tb_alx.setSpeed(M2, CW, 75);`);
            Generator.addCode(`delay(1000*${time});`);
            Generator.addCode(`tb_alx.setSpeed(M1, CW, 0);`);
            Generator.addCode(`tb_alx.setSpeed(M2, CW, 0);`);
        }
    }

    //% block="Στρίψε αριστερά για [TIME] δευτερόλεπτα"
    //% TIME.shadow="number" TIME.defl="2"
    export function turnLeft(parameter: any, block: any) {
        let time = parameter.TIME.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(M1, CCW, 75);`);
            Generator.addCode(`tb_alx.setSpeed(M2, CCW, 75);`);
            Generator.addCode(`delay(1000*${time});`);
            Generator.addCode(`tb_alx.setSpeed(M1, CCW, 0);`);
            Generator.addCode(`tb_alx.setSpeed(M2, CCW, 0);`);
        }
    }
    
    //% block="Σταμάτησε"
    export function moveStop(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(M1, CW, 0);`);
            Generator.addCode(`tb_alx.setSpeed(M2, CCW, 0);`);
        }
    }

    //% block="[MOTOR] κινητήρας, κατεύθυνση [DIRECTION] και ταχύτητα [SPEED]" blockType="command"
    //% MOTOR.shadow="dropdown" MOTOR.options="MOTORS" MOTOR.defl="MOTORS.M1"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTIONS" DIRECTION.defl="DIRECTIONS.CW"
    //% SPEED.shadow="range" SPEED.defl="200" SPEED.params.max="255"
    export function setMotorSpeed(parameter: any, block: any) {
        let motor = parameter.MOTOR.code
        let direction = parameter.DIRECTION.code
        let speed = parameter.SPEED.code
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`tb_alx.setSpeed(${motor}, ${direction}, ${speed});`);
        }
    }

    //% block="Led στο pin [BUTTON] [STATE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.12"
    //% STATE.shadow="dropdown" STATE.options="LED_STATES" BUTTON.defl="LED_STATES.HIGH"
    export function setLedState(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        let state = parameter.STATE.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("TB6612","#include <TB6612FNG.h>");
            Generator.addObject(`TB6612` ,`TB6612FNG`,`tb_alx(5,4,6,7)`);
            Generator.addCode(`digitalWrite(${button}, ${state});`);
        }
    }

    //% block="Κουμπί αφής στο pin [BUTTON], είναι πατημένο;" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.11"
    export function getTactileButtonStatus(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalRead(${button})`);
        }
    }

    //% block="Σέρβο στο pin [BUTTON] όρισε γωνία [ANGLE]" blockType="command"
    //% BUTTON.shadow="dropdown" BUTTON.options="DIGITAL_PORTS" BUTTON.defl="DIGITAL_PORTS.9"
    //% ANGLE.shadow="range" ANGLE.defl="90" ANGLE.params.max="180"
    export function setServoAngle(parameter: any, block: any) {
        let button=parameter.BUTTON.code;
        let angle=parameter.ANGLE.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_servo","#include <DFRobot_Servo.h>");
            Generator.addObject(`servo_object` ,`Servo`,`servo_${button}`);
            Generator.addSetup(`servo_setup`, `servo_${button}.attach(${button});`);
            Generator.addCode(`servo_${button}.angle(${angle});`);
        }
    }
    
    //% block="Διάβασε απόσταση" blockType="reporter"
    export function getDistance(parameter: any, block: any) {
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_URM","#include <DFRobot_URM10.h>");
            Generator.addObject(`DFRobot_URM` ,`DFRobot_URM10`,`alx_distance`);
            Generator.addCode(`(alx_distance.getDistanceCM(A0, 2))`);
        }
    }

    //% block="Διάβασε ένταση ήχου από το μικρόφωνο στο pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PORTS" BUTTON.defl="ANALOG_PORTS.0"
    export function getSoundLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="[SENSOR] αισθητήρας γραμμής, πατάει μαύρη γραμμή;" blockType="boolean"
    //% SENSOR.shadow="dropdown" SENSOR.options="LINE_SENSORS" SENSOR.defl="LINE_SENSORS.A1"
    export function isLineSensorDetectingBlack(parameter: any, block: any) {
        let sensor = parameter.SENSOR.code;
        if(Generator.board === 'arduino'){
            Generator.addCode(`digitalRead(${sensor})`);
        }
    }

    //% block="Διάβασε επίπεδο φωτός από το pin [BUTTON]" blockType="reporter"
    //% BUTTON.shadow="dropdown" BUTTON.options="ANALOG_PINS_LIGHT_SENSOR" BUTTON.defl="ANALOG_PINS_LIGHT_SENSOR.Α6"
    export function getLightLevel(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        button = replace(button);
        if(Generator.board === 'arduino'){
            Generator.addCode(`analogRead(${button})`);
        }
    }

    //% block="Buzzer παίξε νότα [TONE] για [DURATION]" blockType="command"
    //% TONE.shadow="dropdown" TONE.options="BUZZER_TONES" TONE.defl="BUZZER_TONES.131"
    //% DURATION.shadow="dropdown" DURATION.options="BUZZER_TONES_DURATION" DURATION.defl="BUZZER_TONES_DURATION.500"
    export function setBuzzerTone(parameter: any, block: any) {
        let tone = parameter.TONE.code;
        let duration = parameter.DURATION.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("DFRobot_Tone","#include <DFRobot_Libraries.h>");
            Generator.addObject(`DFRobot_Tone` ,`DFRobot_Tone`,`alx_tone`);
            Generator.addCode(`alx_tone.play(13, ${tone}, ${duration});`);
        }
    }

    //% block="Neopixel Led στο pin [PORT], [SUBLEDS] με χρώμα R: [R], G: [G], B: [B] με φωτεινότητα [BRIGHTNESS]" blockType="command"
    //% PORT.shadow="dropdown" PORT.options="DIGITAL_PORTS" PORT.defl="DIGITAL_PORTS.11"
    //% SUBLEDS.shadow="dropdown" SUBLEDS.options="NEOPIXEL_SUB_LEDS" SUBLEDS.defl="NEOPIXEL_SUB_LEDS.4"
    //% R.shadow="range" R.defl="255" R.params.max="255"
    //% G.shadow="range" G.defl="255" G.params.max="255"
    //% B.shadow="range" B.defl="255" B.params.max="255"
    //% BRIGHTNESS.shadow="range" BRIGHTNESS.defl="50" BRIGHTNESS.params.max="255"
    export function setNeopixel(parameter: any, block: any) {
        let port = parameter.PORT.code;
        let leds = parameter.SUBLEDS.code;
        let r = parameter.R.code;
        let g = parameter.G.code;
        let b = parameter.B.code;
        let brightness = parameter.BRIGHTNESS.code;
        if(Generator.board === 'arduino'){
            Generator.addInclude("Adafruit_Neopixel","#include <Adafruit_NeoPixel.h>");
            Generator.addObject(`Adafruit` ,`Adafruit_NeoPixel`,`pixels(4, ${port}, NEO_GRB + NEO_KHZ800);`);
            Generator.addSetup(`neopixel_setup`, `pixels.begin();`);
            if (leds==4){
                Generator.addCode(`pixels.setPixelColor(0, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(1, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(2, pixels.Color(${r}, ${g}, ${b}));`);
                Generator.addCode(`pixels.setPixelColor(3, pixels.Color(${r}, ${g}, ${b}));`);
            } else {
                Generator.addCode(`pixels.setPixelColor(${leds}, pixels.Color(${r}, ${g}, ${b}));`);
            }
            Generator.addCode(`pixels.setBrightness(${brightness});`)
            Generator.addCode(`pixels.show();`)
        }
    }

    //% block="Στείλε στο monitor app το [MESSAGE]" blockType="command"
    //% MESSAGE.shadow="string"
    export function sendToAlxArduinoMonitor(parameter: any, block: any) {
        let message = parameter.MESSAGE.code;
        if(Generator.board === 'arduino'){
            Generator.addSetup(`serial_setup`, `Serial.begin(115200);`);
            Generator.addCode(`Serial.println(${message});`);
            Generator.addCode(`delay(500);`);
        }
    }

    function replace(str :string) {
        return str.replace("+", "");
    }
}

